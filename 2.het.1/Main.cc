#include "ColorSpaces.hh"

int main()
{
	//Írjon programot, amely egy színes kép zöld csatornáján a felhasználó által 
	//megadott küszöbérték alapján sima küszöbölést végez el!
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat I2(I.size(), CV_8UC3);

	for (int i = 0; i < I.rows; ++i) {
		for (int j = 0; j < I.cols; ++j) {
			cv::Vec3b currentPixelGroup = I.at<cv::Vec3b>(i, j);
			currentPixelGroup[ChannelIndexRGB::BLUE] = 0;
			currentPixelGroup[ChannelIndexRGB::RED] = 0;

			I2.at<cv::Vec3b>(i, j) = currentPixelGroup;
		}
	}

	cv::imshow("GreenChannel", I2);

	cv::Mat I4;
	cv::threshold(I2, I4, 160, 255, cv::THRESH_BINARY);
	//legsötétebb rész marad meg csak, többi fehér lesz
	cv::imshow("kuszoboles", I4);
	cv::waitKey();
	
	return 0;
}
