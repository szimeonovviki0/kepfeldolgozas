#include "ColorSpaces.hh"
#include "opencv_headers.hh"


void ColorSpaces::demoConvertColorSpaces()
{
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat gray, hsv;

	cv::cvtColor(I, gray, cv::COLOR_RGB2GRAY);
	cv::cvtColor(I, hsv, cv::COLOR_RGB2HSV);
}

void ColorSpaces::demoModifyColorChannelsRGB()
{
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat I2(I.size(), CV_8UC3);

	uchar modifyValueB = 0;
	uchar modifyValueG = -8; //yellow : 255/242*/0
	uchar modifyValueR = -250;

	for (int i = 0; i < I.rows; ++i) {
		for (int j = 0; j < I.cols; ++j) {
			cv::Vec3b currentPixelGroup = I.at<cv::Vec3b>(i, j);
			checkChannelOverflow(currentPixelGroup[ChannelIndexRGB::BLUE], modifyValueB, ChannelLimits::RGB_B);
			checkChannelOverflow(currentPixelGroup[ChannelIndexRGB::GREEN], modifyValueG, ChannelLimits::RGB_G);
			checkChannelOverflow(currentPixelGroup[ChannelIndexRGB::RED], modifyValueR, ChannelLimits::RGB_R);
			I2.at<cv::Vec3b>(i, j) = currentPixelGroup;
		}
	}

	cv::imshow("Modified", I2);
	cv::waitKey();
}

void ColorSpaces::checkChannelOverflow(uchar& channelValue, uchar n,uchar LIMIT)
{
	if ((channelValue + n) > LIMIT) {
		channelValue = LIMIT;
	}

	else if (channelValue < n) {
		channelValue = 0;
	}

	else {
		channelValue += n;
	}
}