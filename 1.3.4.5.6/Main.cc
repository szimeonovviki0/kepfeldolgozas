#include "MatBasics.hh"

int main()
{
	//1.hét

	//kép beolvasása
	Image I = cv::imread("test.jpg", 0); //szürke kép
	Image I2 = cv::imread("test2.jpg", 1); //színes kép

	//kép megnyítása
	cv::imshow("I", I);
	cv::imshow("I2", I2);





	//2.hét
	//szinezesek
	//kulon fajl


	//3.hét
	//filterek

	cv::Mat I7;
	cv::boxFilter(I, I7, -1,  cv::Size(1, 1)); //elmosódik egy kép a dobozszűrővel.
	//összevonja pixelek
	cv::imshow("I7", I7);

	cv::Mat I3;
	cv::GaussianBlur(I, I3, cv::Size(5, 5), 2); //elmos egy képet Gauss-szűrővel.
	//teljesen hómályósít
	cv::imshow("I3", I3);

	cv::Mat I8;
	cv::medianBlur(I, I8, 1); //elmosódik a kép a medián szűrővel.
	//eltünteti a kis méretű kiugró értékeket, éleket jobban megtartja
	cv::imshow("I8", I8);

	cv::Mat I9;
	cv::equalizeHist(I, I9); 
	//szürke kép kér bemenetként, kimenetnél pedig javítja a kép kontrasztját 
	cv::imshow("I9", I9);

	cv::Mat splitted[3], equalized[3];
	cv::Mat I10;
	cv::split(I2, splitted);
	for (int i = 0; i < 3; ++i)
		cv::equalizeHist(splitted[i], equalized[i]);
	//színes kép kér bemenetként, kimenetnél pedig javítja a kép kontrasztját
	cv::merge(equalized, 3, I10);
	cv::imshow("I10", I10);





	//4.hét
	//küszöbölés

	cv::Mat I4;
	cv::threshold(I, I4, 160, 255, cv::THRESH_BINARY);
	//legsötétebb rész marad meg csak, többi fehér lesz
	cv::imshow("I4", I4);

	cv::Mat I11;
	cv::threshold(I, I11, 160, 255, cv::THRESH_BINARY_INV);
	//legsötétebb rész fehér lesz, többi pedig fekete
	cv::imshow("I11", I11);

	cv::Mat I12;
	cv::threshold(I, I12, 160, 255, cv::THRESH_TRUNC);
	//legsötétebb rész kivágja
	//a küszöbértéknél nagyobb intenzitások a küszöb értékét kapják, 
	//a kisebbek változatlanul megmaradnak.
	cv::imshow("I12", I12);

	cv::Mat I13;
	cv::threshold(I, I13, 160, 255, cv::THRESH_TOZERO);
	//Vágás. A küszöbértéknél nem nagyobbak kinullázódnak. 
	//A magasabbak megmaradnak változatlanul.
	//legsötétebb fekete lesz
	cv::imshow("I13", I13);

	cv::Mat I14;
	cv::threshold(I, I14, 160, 255, cv::THRESH_TOZERO_INV);
	//Vágás.
	//legsötétebb változatlan, többi pedig fekete
	cv::imshow("I14", I14);






	//5.hét
	//ditaláció, erozio
	//morfologia, zajos és apróbb hibás részek eltávolítása
	cv::Mat I5;
	cv::Mat kernel = cv::Mat::ones(cv::Size(5, 5), CV_8UC1);
	cv::dilate(I, I5, kernel); //dilatacio
	//az adott környezetében előforduló intenzitások maximális értékével helyettesítjük
	//tágítás
	cv::imshow("I5", I5);

	cv::Mat I6;
	cv::Mat kernel2 = cv::Mat::ones(cv::Size(3, 3), CV_8UC1);
	cv::erode(I, I6, kernel2); //erozio
	//az adott környezetében előforduló intenzitások minimális értékével helyettesítjük
	//erózió
	cv::imshow("I6", I6);

	cv::Mat I15;
	cv::Mat opened;
	cv::erode(I, I15, kernel2);
	cv::dilate(I15, opened, kernel2);
	//zajok eltávolítása
	cv::imshow("I15", opened);

	cv::Mat I16;
	cv::Mat closed;
	cv::Mat kernel3 = cv::Mat::ones(cv::Size(7, 7), CV_8UC1);
	cv::dilate(I, I16, kernel3);
	cv::erode(I16, closed, kernel3);
	//hasznos az előtérben lévő objektumok belsejében lévő kis lyukak vagy az objektumon 
	//található fekete fekete pontok bezárásakor.
	cv::imshow("I16", closed);





	//6.hét
	//morfológiai operátorok

	//körvonalak kinyerése 
	cv::Mat kernel4 = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
	cv::Mat dilated_rgb, eroded_rgb, contures_rgb;
	cv::Mat dilated_gray, eroded_gray, contures_gray;
	//színes kép
	cv::dilate(I2, dilated_rgb, kernel4); //tágítás
	cv::erode(I2, eroded_rgb, kernel4); //szűkítés
	//szürke kép
	cv::dilate(I, dilated_gray, kernel4);
	cv::erode(I, eroded_gray, kernel4);

	cv::imshow("contures gray", dilated_gray - eroded_gray);
	cv::imshow("contures rgb", dilated_rgb - eroded_rgb);


	//kisebb lyukak detektálása, kiemelése
	//szürke képeknél alkalmazzuk
	cv::Mat kernel5 = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(9, 9));
	cv::Mat eroded1, opened1, dilated1, closed1;
	cv::erode(I, eroded1, kernel5);         //zajok
	cv::dilate(eroded1, opened1, kernel5);  //eltávoítása

	cv::dilate(I, dilated1, kernel5);       //kis lyukak, vagy fekete
	cv::erode(dilated1, closed1, kernel5);  //pontok bezárása, detektálása

	cv::imshow("tophat", I - opened1);
	cv::imshow("blackhat", closed1 - I);


	//körvonalak kiemelése
	cv::Mat dilated_rgb1, eroded_rgb1, contures_rgb1, bolded;
	cv::dilate(I2, dilated_rgb1, kernel4); //tágítás
	cv::erode(I2, eroded_rgb1, kernel4); 
	contures_rgb1 = dilated_rgb1 - eroded_rgb1; //körvonalak kinyerése
	cv::addWeighted(I2, 0.5, contures_rgb1, 0.5, 1, bolded); //addWeighted()-kiszámolja két tömb súlyozott összegét
	//I2 - első tömb; 0.5 - első tömb elemeinek száma; conteres_rgb1 - második tömb; 0.5- második tömb elemeinek a száma; 1,bolded - kimeneti tömb
	cv::imshow("bolded", bolded);


	//kép élesítése
	//színes kép
	cv::Mat blurry, unsharped;
	cv::medianBlur(I2, blurry, 5); //medián szűrő segítségével elmóssa kép
	cv::addWeighted(I2, 2.5, blurry, -1.5, 1, unsharped);
	cv::imshow("unsharped", unsharped);

	cv::waitKey(); // nem zárja be egyből ablakok
	return 0;
}
