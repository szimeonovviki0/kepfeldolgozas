#include "ColorSpaces.hh"
#include "opencv_headers.hh"


void ColorSpaces::demoConvertColorSpaces()
{
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat gray, hsv;

	cv::cvtColor(I, gray, cv::COLOR_RGB2GRAY);
	cv::cvtColor(I, hsv, cv::COLOR_RGB2HSV);
}


void ColorSpaces::demoModifyColorChannelsRGB(int b, int g, int r) 
//b - blue , g - green, r - red
{
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat I2(I.size(), CV_8UC3);

	cv::Mat B = 30;
	cv::Mat G = 20;
	cv::Mat R = 50;

	//szazalek értékek 10%=0,1
	cv::Mat B1 = b/100;
	cv::Mat G1 = g/100;
	cv::Mat R1 = r/100;

	// red -10% green +20% blue +5%
	uchar modifyValueB = B * (1 + B1);
	uchar modifyValueG = G * (1 + G2);
	uchar modifyValueR = R * (1 - R1);

	for (int i = 0; i < I.rows; ++i) {
		for (int j = 0; j < I.cols; ++j) {
			cv::Vec3b currentPixelGroup = I.at<cv::Vec3b>(i, j);
			checkChannelOverflow(currentPixelGroup[ChannelIndexRGB::BLUE],modifyValueB,ChannelLimits::RGB_B);
			checkChannelOverflow(currentPixelGroup[ChannelIndexRGB::GREEN], modifyValueG,ChannelLimits::RGB_G);
			checkChannelOverflow(currentPixelGroup[ChannelIndexRGB::RED], modifyValueR, ChannelLimits::RGB_R);
			I2.at<cv::Vec3b>(i, j) = currentPixelGroup;
		}
	}

	cv::imshow("Modified", I2);
	cv::waitKey();
}


void ColorSpaces::checkChannelOverflow(uchar& channelValue, uchar n,uchar LIMIT)
{
	if ((channelValue + n) > LIMIT) {
		channelValue = LIMIT;
	}

	else if (channelValue < n) {
		channelValue = 0;
	}

	else {
		channelValue += n;
	}
}