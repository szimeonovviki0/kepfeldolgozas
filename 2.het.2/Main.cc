#include "ColorSpaces.hh"

int main()
{
	ColorSpaces colorsHandler;
	
	colorsHandler.demoModifyColorChannelsRGB();
	colorsHandler.demoModifyColorTypeHSV();

	cv::waitKey();
	
	return 0;
}