#include "ColorSpaces.hh"
#include "opencv_headers.hh"


void ColorSpaces::demoConvertColorSpaces()
{
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat gray, hsv;

	cv::cvtColor(I, gray, cv::COLOR_RGB2GRAY);
	cv::cvtColor(I, hsv, cv::COLOR_RGB2HSV);
}

void ColorSpaces::demoModifyColorTypeHSV()
{
	cv::Mat I = cv::imread("test.jpg", 1);
	cv::Mat I2(I.size(), CV_8UC3);
	cv::Mat I_converted, I_converted2;
	uchar modifyValue = +50;
	int colorModifyRange_min = 150;
	int colorModifyRange_max = 179;

	cv::cvtColor(I, I_converted, cv::COLOR_RGB2HSV);

	for (int i = 0; i < I.rows; ++i) {
		for (int j = 0; j < I.cols; ++j) {
			cv::Vec3b currentPixelGroup = I_converted.at<cv::Vec3b>(i, j);

			if ((currentPixelGroup[ChannelIndexHSV::HUE] >= colorModifyRange_min) &&
				(currentPixelGroup[ChannelIndexHSV::HUE] <= colorModifyRange_max))
			{
				//currentPixelGroup[ChannelIndexHSV::VALUE] += 30;
				checkChannelOverflow(currentPixelGroup[ChannelIndexHSV::SATURATION], modifyValue, ChannelLimits::HSV_V);
			}

			I2.at<cv::Vec3b>(i, j) = currentPixelGroup;
		}
	}

	cv::cvtColor(I2, I_converted2, cv::COLOR_HSV2RGB);
	cv::imshow("Origin", I);
	cv::imshow("Modified", I_converted2);
	cv::waitKey();
}

void ColorSpaces::checkChannelOverflow(uchar& channelValue, uchar n,uchar LIMIT)
{
	if ((channelValue + n) > LIMIT) {
		channelValue = LIMIT;
	}

	else if (channelValue < n) {
		channelValue = 0;
	}

	else {
		channelValue += n;
	}
}