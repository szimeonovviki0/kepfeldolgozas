#include "MatBasics.hh"

int main()
{
	
	//Írjon programot, amely zajos képen globális küszöbölést végez el!
	//Előfeldolgozásként alkalmazzon szűrőket(box, gauss, vagy medián)!
	Image I = cv::imread("test.jpg", 1);

	cv::Mat I7;
	cv::boxFilter(I, I7, -1, cv::Size(1, 1)); 
	cv::imshow("box_filter", I7);

	cv::Mat I12;
	cv::threshold(I7, I12, 160, 255, cv::THRESH_TRUNC);
	cv::imshow("kuszobles", I12);

	cv::waitKey();
	return 0;
}